package fit.may.scycle;

import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbAccessory;
import android.hardware.usb.UsbManager;
import android.os.ParcelFileDescriptor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.lang.String;

import static java.lang.System.in;

public class MainActivity extends AppCompatActivity {

    TextView mText;
    TextView mText2;
    ParcelFileDescriptor mFileDescriptor;
    String mMessage;
    String mMessage2;
    String TAG = "SCycle";
    double dia = 0.0007;
    boolean _s = false;
    boolean _c = false;
    double dist_cov;

    String startTimeHr;
    String startTimeMin;
    String endTimeHr;
    String endTimeMin;
    String avgSpeed;
    String calories;

    long start;
    long end;

    UsbManager mUsbManager;
    UsbAccessory mAccessory;

    FileInputStream mInputStream;
    FileOutputStream mOutputStream;

    Runnable mUpdateUI = new Runnable() {
        @Override
        public void run() {
            mText.setText(mMessage);
        }
    };

    Runnable mUpdateUI2 = new Runnable() {
        @Override
        public void run() {
            mText2.setText(mMessage2);
        }
    };

    /** Called when the user clicks the Decrease or Increase button */
    public void sendMessage(View view) {

        char direction = 0;

        byte[] buffer = new byte[5];
        buffer[0] = (byte) 'A';
        for (int i = 1; i < 5; i++) {
            buffer[i] = (byte) direction;
        }

        mMessage2 = "<<< ";
        try {
            mOutputStream.write(buffer);
        } catch (IOException e) {
            e.printStackTrace();
            mMessage2 += "Send error";
        }
        //String msg = new String(buffer);
        //mMessage2 += msg + System.getProperty("line.separator");
        //mText.post(mUpdateUI2);

    }

    Runnable mListenerTask = new Runnable() {
        @Override
        public void run() {

            byte[] buffer = new byte[8];
            int ret;

            try {
                //mMessage = ">>> ";
                ret = mInputStream.read(buffer);
                if (ret >= 4) {
                    String msg = new String(buffer);
                    String spd = new String();
                    String dist = new String();
                    for (int i=0; i<msg.length();i++) {
                        char c = msg.charAt(i);
                        if(c=='s'){
                            _s = true;
                            _c = false;
                        }
                        else if(c=='c'){
                            _c = true;
                            _s = false;
                        }
                        else{
                            if(_s){
                                spd+=c;
                            }
                            else{
                                dist+=c;
                            }
                        }
                    }
                    mMessage = spd;
                    double dist_doub = Double.parseDouble(dist);
                    dist_cov = dist_doub*dia*3.14;
                    mMessage2 = Double.toString(dist_cov);
                } else {
                    mMessage = "__";
                }

            } catch (IOException e) {
                e.printStackTrace();
                mMessage = "__";
            }

            //mMessage += System.getProperty("line.separator");
            mText.post(mUpdateUI);
            mText2.post(mUpdateUI2);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            new Thread(this).start();
        }
    };

    public void stopRide(View view){
        end = System.currentTimeMillis();
        Date date = new Date(start-3600000);
        Date dateDiff = new Date(end-3600000) ;
        SimpleDateFormat fileName = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
        String dispTime = dateFormat.format(dateDiff);
        String file_name = fileName.format(date);
        startTimeHr = file_name.substring(11,12);
        startTimeMin = file_name.substring(14,15);
        endTimeHr = dispTime.substring(11,12);
        endTimeMin = dispTime.substring(14,15);
        long timeDiff = end-start-3600000;
        double time = timeDiff/3600000;
        double avgSpeed = (Math.round((dist_cov/time)*100))/100;
        Double calories = dist_cov*2.5;
        try{
            FileOutputStream file = openFileOutput(file_name,Context.MODE_PRIVATE);
            String write_str = 'a'+startTimeHr+'b'+startTimeMin+'c'+Double.toString(avgSpeed)+'d'+endTimeHr+'e'+endTimeMin+'f'+mMessage2;
            file.write(write_str.getBytes());
            Log.d("SCycle","data " + write_str);
            //file.write(startTimeHr.getBytes());
            //file.write(startTimeMin.getBytes());
            //file.write(mMessage2.getBytes());
            //file.write(Double.toString(avgSpeed).getBytes());
            //file.write(endTimeHr.getBytes());
            //file.write(endTimeMin.getBytes());

            file.close();
        }
        catch(Exception e){
            Log.d("SCycle","exception");
            e.printStackTrace();
        }


        Intent intent = new Intent(this,stats_acivity.class);
        intent.putExtra("sh",startTimeHr);
        intent.putExtra("sm",startTimeMin);
        intent.putExtra("sp",Double.toString(avgSpeed));
        intent.putExtra("cal",Double.toString(calories));
        intent.putExtra("dist",mMessage2);
        intent.putExtra("eh",endTimeHr);
        intent.putExtra("em",endTimeMin);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mText = (TextView) findViewById(R.id.speed);
        mText.setMovementMethod(new ScrollingMovementMethod());

        mText2 = (TextView) findViewById(R.id.distance);

        Intent intent = getIntent();
        mUsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        mAccessory = intent.getParcelableExtra(UsbManager.EXTRA_ACCESSORY);

        if (mAccessory == null) {
            //mText.append("Not started by the accessory directly" +
            //        System.getProperty("line.separator"));
            Intent newIntent = new Intent(this,stats_acivity.class);
            startActivity(newIntent);
            return;
        }

        Log.v(TAG, mAccessory.toString());
        mFileDescriptor = mUsbManager.openAccessory(mAccessory);
        if (mFileDescriptor != null) {
            FileDescriptor fd = mFileDescriptor.getFileDescriptor();
            mInputStream = new FileInputStream(fd);
            mOutputStream = new FileOutputStream(fd);
        }
        start = System.currentTimeMillis();
        Log.v(TAG, mFileDescriptor.toString());
        new Thread(mListenerTask).start();
    }
}
