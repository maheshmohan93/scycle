package fit.may.scycle;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.os.*;
import android.view.animation.DecelerateInterpolator;

import java.io.File;
import java.io.FileInputStream;

import org.w3c.dom.Text;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link stats_acivity_1.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link stats_acivity_1#newInstance} factory method to
 * create an instance of this fragment.
 */
public class stats_acivity_1 extends Fragment {

    TextView curDist;
    TextView avgSpeed;
    TextView startTimeHr;
    TextView startTimeMin;
    TextView endTimeHr;
    TextView endTimeMin;
    TextView calories;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "00";
    private static final String ARG_PARAM2 = "00";

    private static final String ARG_PARAM3 = "00";
    private static final String ARG_PARAM4 = "00";
    private static final String ARG_PARAM5 = "0";
    private static final String ARG_PARAM6 = "0";
    private static final String ARG_PARAM7 = "0";

    // TODO: Rename and change types of parameters
    private String mParam1 = "0.1234";
    private String mParam2 = "7.234";
    private String mParam3 = "";
    private String mParam4;
    private String mParam5;
    private String mParam6;
    private String mParam7;

    private OnFragmentInteractionListener mListener;

    public stats_acivity_1() {
        // Required empty public constructor
    }

    public void animateTextView(int initialValue, int finalValue, final TextView textview) {
        DecelerateInterpolator decelerateInterpolator = new DecelerateInterpolator(0.0f);
        int start = Math.min(initialValue, finalValue);
        int end = Math.max(initialValue, finalValue);
        int difference = Math.abs(finalValue - initialValue);
        Handler handler = new Handler();
        for (int count = start; count <= end; count++) {
            int time = Math.round(decelerateInterpolator.getInterpolation((((float) count) / difference)) * 100) * count;
            final int finalCount = ((initialValue > finalValue) ? initialValue - count : count);
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    textview.setText(finalCount + "");
                }
            }, time);
        }
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment stats_acivity_1.
     */
    // TODO: Rename and change types and number of parameters
    public static stats_acivity_1 newInstance(String param1, String param2, String p3, String p4, String p5, String p6, String p7) {
        stats_acivity_1 fragment = new stats_acivity_1();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putString(ARG_PARAM3, p3);
        args.putString(ARG_PARAM4, p4);
        args.putString(ARG_PARAM5, p5);
        args.putString(ARG_PARAM6, p6);
        args.putString(ARG_PARAM7, p7);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);

            mParam3 = getArguments().getString(ARG_PARAM3);
            mParam4 = getArguments().getString(ARG_PARAM4);
            mParam5 = getArguments().getString(ARG_PARAM5);
            mParam6 = getArguments().getString(ARG_PARAM6);
            mParam7 = getArguments().getString(ARG_PARAM7);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View myInflatedView =  inflater.inflate(R.layout.fragment_stats_acivity_1,container,false);

        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_stats_acivity_1, container, false);
        return myInflatedView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        curDist = (TextView) getView().findViewById(R.id.cur_dist);
        avgSpeed = (TextView) getView().findViewById(R.id.cur_speed);
        startTimeHr = (TextView) getView().findViewById(R.id.start_hr);
        startTimeMin = (TextView) getView().findViewById(R.id.start_min);
        endTimeHr = (TextView) getView().findViewById(R.id.end_hr);
        endTimeMin = (TextView) getView().findViewById(R.id.end_min);
        calories = (TextView) getView().findViewById(R.id.cur_cal);


        //curDist.setText("25");
        Log.d("frag_1","setText");
        String fileDist = new String();
        String fileSpeed = new String();
        String filestartHr = new String();
        String fileStartMin = new String();
        String fileEndHr = new String();
        String fileEndMin = new String();

        boolean dFlag = false;
        boolean sFlag = false;
        boolean shFlag = false;
        boolean smFlag = false;
        boolean ehFlag = false;
        boolean emFlag = false;

        byte[] stream = new byte[25];

        /*String[] savedFiles;
        savedFiles = getActivity().getApplicationContext().fileList();
        int count = savedFiles.length;
        String fileName = savedFiles[count-1];
        try{
            FileInputStream input = getContext().openFileInput(fileName);

            int count_1 = input.read(stream);
            Log.d("frag_1",stream.toString());

            for(int j = 0;j<stream.toString().length();j++){
                char c = stream.toString().charAt(j);
                if (c == 'a'){
                    shFlag = true;
                    dFlag = false;
                    sFlag = false;
                    smFlag = false;
                    ehFlag = false;
                    emFlag = false;
                }
                else if (c == 'b'){
                    shFlag = false;
                    dFlag = false;
                    sFlag = false;
                    smFlag = true;
                    ehFlag = false;
                    emFlag = false;
                }
                else if(c=='c'){
                    shFlag = false;
                    dFlag = false;
                    sFlag = true;
                    smFlag = false;
                    ehFlag = false;
                    emFlag = false;
                }
                else if (c=='d') {
                    shFlag = false;
                    dFlag = false;
                    sFlag = false;
                    smFlag = false;
                    ehFlag = true;
                    emFlag = false;
                }
                else if (c=='e'){
                    shFlag = false;
                    dFlag = false;
                    sFlag = false;
                    smFlag = false;
                    ehFlag = false;
                    emFlag = true;
                }
                else if (c=='f'){
                    shFlag = false;
                    dFlag = true;
                    sFlag = false;
                    smFlag = false;
                    ehFlag = false;
                    emFlag = false;
                }
                else{
                    if(shFlag){
                        filestartHr+=c;
                    }
                    else if(dFlag){
                        fileDist+=c;
                    }
                    else if (sFlag){
                        fileSpeed+=c;
                    }
                    else if(smFlag){
                        fileStartMin+=c;
                    }
                    else if(ehFlag){
                        fileEndHr+=c;
                    }
                    else{
                        fileEndMin+=c;
                    }
                }
            }
            Log.d("frag_1",fileDist);
            curDist.setText(fileDist);
            Log.d("frag_1",fileSpeed);
            avgSpeed.setText(fileSpeed);
            startTimeHr.setText(filestartHr);
            startTimeMin.setText(fileStartMin);
            endTimeHr.setText(fileEndHr);
            endTimeMin.setText(fileEndMin);
        }
        catch(Exception e){
            e.printStackTrace();
        }*/

        //animateTextView(0,Integer.parseInt(),curDist);
        curDist.setText(mParam1);
        //Log.d("frag_1",mParam1.substring(0,1));
        avgSpeed.setText(mParam2);
        //Log.d("frag_1",mParam2.substring(0,1));
        startTimeHr.setText(mParam3);
        startTimeMin.setText(mParam4);
        endTimeHr.setText(mParam5);
        endTimeMin.setText(mParam6);
        calories.setText(mParam7);
        //Log.d("frag_1",mParam7.substring(0,1));

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
