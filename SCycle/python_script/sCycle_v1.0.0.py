	#!/usr/bin/env python
# coding=utf-8
#
#

"""
Simple Android Accessory client in Python.
"""

import usb.core
import sys
import time
import random
import RPi.GPIO as GPIO
import math
import os

        # Google Vendor ID
GOOGLE_ACCESSORY = 11521

        #Enter the device VID and PID of your android device
VID_ANDROID_ACCESSORY = 8888
PID_ANDROID_ACCESSORY = 11894


        # Global Variables
i = 0   # Debug variable
pi = 3.14
lastTime = 0
finalSpeed = 0
       #wheelRad = input("Enter tyre radius in meters:")
wheelRad = 0.358        # in meters # 700C wheels
wheelRotation = 0
watchCat = 0
wheelCirc = 2*pi*wheelRad
diffTime = 10*wheelCirc

        # Set Default Lock pin as 0
GPIO.setmode(GPIO.BCM)
GPIO.setup(27,GPIO.OUT)
GPIO.output(27,False)

        # GPIO 22 SET as input for starting the system
GPIO.setup(22,GPIO.IN, pull_up_down=GPIO.PUD_DOWN)


 

##############################################################################
#	Sensor Side	

def userLoop():
	time.sleep(1)
	print "\n\nSmart Cycle Module"
# Lock the cycle in case of unexpected failure
        GPIO.output(27,False)
	if(GPIO.input(22)):
        	print "Module started Succesfully"
		secondry_main()
	else:
		print "Waiting for User Input......"
		userLoop()

# Initialize GPIO Ports
def portInitialize():
	    print "Initializing RPi ports"
 
       
# Module to find wheel speed from time elapsed
def speedometer(circum,timeElapsed):
        global finalSpeed
	# speed in meter per secomd
        tempSpeed1 = circum/timeElapsed
        # speed in kilometer per hour
        tempSpeed2 = (tempSpeed1*18)/5
        finalSpeed = math.floor(tempSpeed2)
	return finalSpeed

# Initialise thread for detection 17 going high  
def my_callback(channel):
	global watchCat
        global diffTime
        global lastTime
        global wheelRotation
	print "rising edge detected on 17"  
        thisTime = time.time()
        diffTime = thisTime - lastTime
        lastTime = thisTime
	wheelRotation += 1
	watchCat = 20

def get_accessory_dev(ldev):
    """Trigger accessory mode and send the dev handler"""
    set_protocol(ldev)
    set_strings(ldev)
    set_accessory_mode(ldev)
    adev = usb.core.find(find_all = True)


    if adev:
        print "Android accessory mode started"
    return adev


def get_android_dev():
    """Look for a potential Android device"""

    ldev = usb.core.find(bDeviceClass=0)
 
    if ldev:
	# give time for a mount by the OS
        time.sleep(2)
        # request again a device handler

        ldev = usb.core.find(bDeviceClass=0)

        print "Device found"
        print "VID: {:#04x} PID: {:#04x}".format(
            ldev.idVendor,
            ldev.idProduct
        )
    return ldev


def set_protocol(ldev):
    """Set the USB configuration"""
    try:
        ldev.set_configuration()
    except usb.core.USBError as e:
        if  e.errno == 16:
            pass
        else:
            sys.exit(e)
    ret = ldev.ctrl_transfer(0xC0, 51, 0, 0, 2)
    protocol = ret[0]
    print "Protocol version: {}".format(protocol)
    return


def set_strings(ldev):
    """Send series of strings to activate accessory mode"""
    send_string(ldev, 0, 'MAY')
    send_string(ldev, 1, 'SCycle')
    send_string(ldev, 2, 'A Python based Android accessory')
    send_string(ldev, 3, '1.0')
    send_string(
        ldev,
        4,
        'https://github.com/Arn-O/py-android-accessory/'
    )
    return


def set_accessory_mode(ldev):
    """Trigger the accessory mode"""
    ret = ldev.ctrl_transfer(0x40, 53, 0, 0, '', 0)    
    assert not ret
    time.sleep(1)
    return


def send_string(ldev, str_id, str_val):
    """Send a given string to the Android device"""
    ret = ldev.ctrl_transfer(0x40, 52, 0, str_id, str_val, 0)
    assert ret == len(str_val)
    return 


def sensor_variation(toss):
    """Return sensor variation"""
    return {
        -10: -1,
        10: 1
    }.get(toss, 0)


def sensor_output(lsensor, variation):
    """Keep the sensor value between 0 and 100"""
    output = lsensor + variation
    if output < 0:
        output = 0
    else:
        if output > 100:
            output = 100
    return output


def communication_loop(ldev):
    """Accessory client to device communication loop"""
    global wheelRotation
    global finalSpeed	 
    global wheelCirc
    global diffTime
    global watchCat

    while True:
	watchCat -= 1
	if(watchCat == 0 ):
		print "watchCat:",watchCat
		finalSpeed = 0
		diffTime = 10*wheelCirc
		print "Vehicle Stopped !!"

	else:
		print "Vehicle Running..."
	speedometer(wheelCirc,diffTime)
#	payLoad = finalSpeed # wheelRotation
        
        # write to device
        msg1 = "s{0:d}".format(int(finalSpeed))
	msg2= "c{0:d}".format(wheelRotation)
	msg = msg1 + msg2
	print "speed:{}".format(msg1)
	print "Payload:{}:".format(msg)
        try:
            ret = ldev.write(0x02, msg, 150)
 	    print "Send data to app:"
            assert ret == len(msg)
        except usb.core.USBError as e:
	    print "Couldn't send to app !"
            print e.errno
            if e.errno == 19:
		print "Error 19 Occured !\nSystem Reeboot"
	        GPIO.output(27,False)
		userLoop()

                break
            if e.errno == 110:
                print "Error 110....app closed before ejection"
 #               userLoop()

                # the application has been stopped
                break

        # read from device
        try:
	    print "Trying to read from the app:"
            ret = ldev.read(0x81, 5, 150)
            sret = ''.join([chr(x) for x in ret])
            print ">>> {}".format(sret)
            if sret == "A1111":
                 variation = -3  
            else:
                if sret == "A0000":
                    variation = 3 
            sensor = sensor_output(sensor, variation)
        except usb.core.USBError as e:
	    print "No Data sent from the app"
            if e.errno == 19:
               GPIO.output(27,False)
               userLoop()

               break
            if e.errno == 110:
		print "Error 110...app close before ejection"
#		userLoop()

                # a timeout is OK, no message has been sent
                pass
            else:
                print e	
	print "\n"

    return

def rebooot():
	print "System Rebooting"
	os.system('sudo shutdown -r now')

def secondry_main():
    """Where everything starts"""	
 
    portInitialize()
	
    print "Looking for an Android device"
    while True:
	tdev = usb.core.find(bDeviceClass=0)
	print "Product ID:", tdev.idProduct
#	print "pid:",PID_ANDROID_ACCESSORY
        if (tdev.idProduct == PID_ANDROID_ACCESSORY):	
	        tadev = get_accessory_dev(tdev)
		continue

	ddev = get_android_dev()
        adev = get_accessory_dev(ddev)
        print "adev:",adev
        print "Communicating with new Instance"

	GPIO.output(27,True)
	print "Unlocked .................."

        communication_loop(ddev)
        break

def main():
	# Set Default Lock pin as 0
	
       # GPIO 17 as imput  
	GPIO.setup(17, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

        # detect falling edge in the port 17  
	GPIO.add_event_detect(17, GPIO.FALLING, callback=my_callback, bouncetime=300)

	

	userLoop()

if __name__ == '__main__':
    main()
